var weddingProject = angular.module('weddingProject', [
    'ngRoute',
    'weddingProjectControllers',
    'weddingProjectServices',
    'ngMaterial',
    'lfNgMdFileInput',
    'materialCalendar',
    'ui.tinymce',
    'ngSanitize'
]);

weddingProject.config(['$routeProvider', function($routeProvider) {


    $routeProvider.
    when('/admin', {
        templateUrl: 'partials/login.html',
        controller: 'LoginController'
    }).
    when('/admin/blog', {
        templateUrl: 'partials/admin/blog-admin.html',
        controller: 'BlogAdminController'
    }).
    when('/admin/portfolio', {
        templateUrl: 'partials/admin/portfolio-admin.html',
        controller: 'PortfolioAdminController'
    }).
    when('/admin/slider', {
        templateUrl: 'partials/admin/slider-admin.html',
        controller: 'SliderAdminController'
    }).
    when('/admin/calendar', {
        templateUrl: 'partials/admin/calendar-admin.html',
        controller: 'CalendarAdminController'
    }).
    when('/', {
        templateUrl: 'partials/index.html',
        controller: 'MainController',
        activetab: 'home'
    }).
    when('/portfolio', {
        templateUrl: 'partials/portfolio.html',
        controller: 'PortfolioController',
        activetab: 'portfolio'
    }).
    when('/portfolio/:portfolioId', {
        templateUrl: 'partials/portfolio-single-view.html',
        controller: 'PortfolioSingleController',
        activetab: 'portfolio'
    }).
    when('/calendar', {
        templateUrl: 'partials/calendar.html',
        controller: 'CalendarController',
        activetab: 'calendar'
    }).
    when('/blog', {
        templateUrl: 'partials/blog.html',
        controller: 'BlogController',
        activetab: 'blog'
    }).
    when('/blog/:blogId', {
        templateUrl: 'partials/blog-single-view.html',
        controller: 'BlogSingleController',
        activetab: 'blog'
    }).
    when('/contact', {
        templateUrl: 'partials/contact.html',
        controller: 'ContactController',
        activetab: 'contact'
    }).
    when('/privacy-policy', {
        templateUrl: 'partials/privacy-policy.html',
        controller: 'PrivacyPolicyController',
        activetab: 'privacy-policy'
    }).
    otherwise({
        redirectTo: '/',
    });

}]);
