var weddingProjectControllers = angular.module('weddingProjectControllers', []);


weddingProjectControllers.controller('LoginController', ['$scope', '$location', 'userService', '$mdToast', function ($scope, $location, userService, $mdToast) {
    
    $scope.email = '';
    $scope.password = '';

    if(userService.checkIfLoggedIn()) {
        $location.path('/admin/blog');
    }

    $scope.login = function() {
        if($scope.email == '' && $scope.password == '') {
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Enter your email and password!')
                    .position('bottom left')
                    .hideDelay(1000)
            );
        } else if($scope.email == '' && $scope.password != '') {
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Enter your email!')
                    .position('bottom left')
                    .hideDelay(1000)
            );
        } else if($scope.email != '' && $scope.password == '') {
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Enter your password!')
                    .position('bottom left')
                    .hideDelay(1000)
            );
        }
        else {
            userService.login(
                $scope.email, $scope.password,
                function(response){
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Successfully logged in!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                    $location.path('/admin/blog');
                },
                function(response){
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Wrong username or password!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                }
            );
        }
    }

    $scope.pressEnter = function(keyEvent) {
        if (keyEvent.which === 13) {
            if($scope.email == '' && $scope.password == '') {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Enter your email and password!')
                        .position('bottom left')
                        .hideDelay(1000)
                );
            } else if($scope.email == '' && $scope.password != '') {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Enter your email!')
                        .position('bottom left')
                        .hideDelay(1000)
                );
            } else if($scope.email != '' && $scope.password == '') {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Enter your password!')
                        .position('bottom left')
                        .hideDelay(1000)
                );
            }
            else {
                userService.login(
                    $scope.email, $scope.password,
                    function(response){
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Successfully logged in!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                        $location.path('/admin/blog');
                    },
                    function(response){
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Wrong username or password!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                    }
                );
            }
        }
    }

}]);

weddingProjectControllers.controller('MainController', ['$scope', '$location', function ($scope, $location) {

    $('#camera').camera({
                autoAdvance: true,
                height: '42.8293%',
                minHeight: '300px',
                pagination: false,
                thumbnails: false,
                playPause: false,
                hover: false,
                loader: 'none',
                navigation: false,
                navigationHover: false,
                mobileNavHover: false,
                fx: 'simpleFade'
            })

}]);

weddingProjectControllers.controller('CalendarController', ['$scope','$rootScope', '$location', '$filter', '$http', '$q', function ($scope, $rootScope, $location, $filter, $http, $q) {
 	$scope.selectedDate = null;
    $scope.firstDayOfWeek = 0;


    $scope.setDirection = function(direction) {
      $scope.direction = direction;
    };
    $scope.dayClick = function(date) {
      $scope.msg = "Requested date: " + $filter("date")(date, "MMM d, y");
      $rootScope.contactMessage = $scope.msg;
      $location.path("/contact");
    };

    $scope.prevMonth = function(data) {
      $scope.msg = "You clicked (prev) month " + data.month + ", " + data.year;
    };
    $scope.nextMonth = function(data) {
      $scope.msg = "You clicked (next) month " + data.month + ", " + data.year;
    };
    $scope.setDayContent = function(date) {

        return "<p></p>";

    };
    $scope.tooltips = true;

}]);

weddingProjectControllers.controller('PortfolioController', ['$scope', '$location', 'HttpGetServices', 'parseHtmlService', function ($scope, $location, HttpGetServices, parseHtmlService) {

    HttpGetServices.getListData(HttpGetServices.endpointPortfolioList)
        .then(function(data) {
            $scope.portfolios = data;
    });

    $scope.parseHtml = function(data) {
        return parseHtmlService.parse(data);
    };

}]);

weddingProjectControllers.controller('PortfolioSingleController', ['$scope', '$location', '$routeParams', 'HttpGetServices', 'parseHtmlService', function ($scope, $location, $routeParams, HttpGetServices, parseHtmlService) {

    var id = $routeParams.portfolioId;

    HttpGetServices.getSingleData(HttpGetServices.endpointSinglePortfolio, id)
        .then(function(data) {
            $scope.portfolio = data;

            $scope.parseHtml = function() {
                return parseHtmlService.parse(data.description);
            };
    });


}]);

weddingProjectControllers.controller('BlogController', ['$scope', '$location', 'HttpGetServices', 'parseHtmlService', function ($scope, $location, HttpGetServices, parseHtmlService) {

    HttpGetServices.getListData(HttpGetServices.endpointBlogList)
        .then(function(data) {
            $scope.blogs = data;
    });

    $scope.parseHtml = function(data) {
        return parseHtmlService.parse(data);
    };

}]);

weddingProjectControllers.controller('BlogSingleController', ['$scope', '$location', '$routeParams', 'HttpGetServices', 'parseHtmlService', function ($scope, $location, $routeParams, HttpGetServices, parseHtmlService) {

    var id = $routeParams.blogId;

    HttpGetServices.getSingleData(HttpGetServices.endpointSingleBlog, id)
        .then(function(data) {
            $scope.blog = data;

            $scope.parseHtml = function() {
                return parseHtmlService.parse(data.body);
            };
    });


}]);


weddingProjectControllers.controller('ContactController', ['$scope','$rootScope', function ($scope,$rootScope) {
    $scope.contactMessage = $rootScope.contactMessage;
}]);

weddingProjectControllers.controller('PrivacyPolicyController', ['$scope', function ($scope) {

}]);

weddingProjectControllers.controller('BladeController', ['$scope', '$location', '$rootScope', '$route', 'HideElementsService', function ($scope, $location, $rootScope, $route, HideElementsService) {

    HideElementsService.hideElements();

      $rootScope.$on('$routeChangeSuccess', function(e, current, pre) {
        console.log('Current route name: ' + $location.path());
        $scope.$route = $route;

        HideElementsService.hideElements();

      });

}]);

// ADMIN PANEL
weddingProjectControllers.controller('AdminMenuController', ['$scope', '$location', 'userService','$mdSidenav', function ($scope, $location, userService, $mdSidenav) {
    $scope.logOut = function(){
        userService.logout();
        $location.path('/admin');
    }

    $scope.goto = function(root) {
        $location.path(root);
    }
    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');

    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
      }
    }
}]);

weddingProjectControllers.controller('BlogAdminController', ['$scope', '$location', 'HttpGetServices', '$mdDialog', '$rootScope', 'userService', 'HttpPostServices', 'parseHtmlService', '$mdToast', function ($scope, $location, HttpGetServices, $mdDialog, $rootScope, userService, HttpPostServices, parseHtmlService, $mdToast) {

    if(userService.checkIfLoggedIn() == true) {

        $scope.currentNavItem = 'blog';
        HttpGetServices.getListData(HttpGetServices.endpointBlogList)
            .then(function(data) {
                $scope.blogs = data;
            });

        $scope.parseHtml = function(data) {
            return parseHtmlService.parse(data);
        };

        $scope.addBlog = function(ev) {

            if(userService.checkIfLoggedIn() == true) {
                $mdDialog.show({
                    controller: 'AddBlogPopUpController',
                    templateUrl: 'partials/admin/popups/add-blog-pop-up.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen
                })
                .then(function(answer) {
                    if(answer == 'add') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Successfully added new blog!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                        HttpGetServices.getListData(HttpGetServices.endpointBlogList)
                            .then(function(data) {
                                $scope.blogs = data;
                                console.log(data);
                            });
                    }
                    if(answer == 'logout') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('You have to login first!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                    }
                }, function() {
                    console.log('You cancelled the dialog.');
                });

            } else {
                userService.logout();
                $location.path('/admin');
            }
        };

        $scope.editBlog = function(ev, id) {

            if(userService.checkIfLoggedIn() == true) {
                $rootScope.editBlogId = id;
                $mdDialog.show({
                    controller: 'EditBlogPopUpController',
                    templateUrl: 'partials/admin/popups/edit-blog-pop-up.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen
                })
                .then(function(answer) {
                    if(answer == 'edit') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Successfully edited blog!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                        HttpGetServices.getListData(HttpGetServices.endpointBlogList)
                            .then(function(data) {
                                $scope.blogs = data;
                                console.log(data);
                            });
                    }
                    if(answer == 'logout') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('You have to login first!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                    }
                }, function() {
                    console.log('You cancelled the dialog.');
                });

            } else {
                userService.logout();
                $location.path('/admin');
            }
        
        };

        $scope.deleteBlog = function(ev, id) {

            if(userService.checkIfLoggedIn() == true) {
                $rootScope.deleteBlogId = id;
                $mdDialog.show({
                    controller: 'DeleteBlogPopUpController',
                    templateUrl: 'partials/admin/popups/delete-blog-pop-up.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: $scope.customFullscreen
                })
                .then(function(answer) {
                  console.log(answer);
                  if (answer == 'yes') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Successfully deleted blog!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                        HttpGetServices.getListData(HttpGetServices.endpointBlogList)
                            .then(function(data) {
                                $scope.blogs = data;
                                console.log(data);
                            });
                  }
                  if(answer == 'logout') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('You have to login first!')
                                .position('bottom left')
                                .hideDelay(1000)
                        );
                   }
                }, function() {
                  console.log(answer);
                });
            } else {
                userService.logout();
                $location.path('/admin');
            }
    
        };

    } else {
        userService.logout();
        $location.path('/admin');
    }

}]);

weddingProjectControllers.controller('AddBlogPopUpController', ['$scope', 'HttpPostServices', 'userService', '$mdDialog', '$mdToast', '$location', function ($scope, HttpPostServices, userService, $mdDialog, $mdToast, $location) {

    $scope.answer = function(answer) {
        console.log(answer);
        if(userService.checkIfLoggedIn() == true) {

            if(answer == 'add') {
                var title = $scope.title;
                var body = $scope.tinymceModel;

                var imageFile = new FormData();
                angular.forEach($scope.files, function(obj) {
                    imageFile.append('file', obj.lfFile);
                });

                console.log(title, body, imageFile.get('file'));
                if((title == null || title == "") && (body == null || body == "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Title and Description!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if((title == null || title == "") && (body != null || body != "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Title!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if((title != null || title != "") && (body == null || body == "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Description!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else {
                    var params = {
                        title: title,
                        body: body
                    };

                    var token = userService.getCurrentToken();
                    console.log(params);
                    HttpPostServices.postData(HttpPostServices.endpointAddNewBlog, params, token,
                        function(response) {
                            console.log("Success: " + response);
                        },
                        function(response) {
                            console.log("Error: " + response);
                        }
                    );

                    $mdDialog.hide(answer);
                }
            } else if(answer == 'close') {
                $mdDialog.hide(answer);
            }

        } else {
            $mdDialog.hide('logout');
            userService.logout();
            $location.path('/admin');
        }
    };

    $scope.tinymceOptions = {
        plugins: 'link image code',
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code',
        skin: 'lightgray',
        theme : 'modern',
        file_browser_callback: function(field_name, url, type, win) {
            console.log('file_browser_callback called!');
        }
    };

}]);

weddingProjectControllers.controller('EditBlogPopUpController', ['$scope', '$rootScope', 'HttpGetServices', 'HttpPostServices', 'userService', '$mdDialog', '$mdToast', '$location',  function ($scope, $rootScope, HttpGetServices, HttpPostServices, userService, $mdDialog, $mdToast, $location) {

    var id = $rootScope.editBlogId;

    HttpGetServices.getSingleData(HttpGetServices.endpointSingleBlog, id)
        .then(function(data) {
            console.log(data);
            $scope.tinymceModel = data.body;
            $scope.title = data.title;
    });

    $scope.answer = function(answer) {
        console.log(answer);

        if(userService.checkIfLoggedIn() == true) {

            if(answer == 'edit') {
                var id = $rootScope.editBlogId;
                var title = $scope.title;
                var body = $scope.tinymceModel;

                if((title == null || title == "") && (body == null || body == "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Title and Description!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if((title == null || title == "") && (body != null || body != "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Title!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else if((title != null || title != "") && (body == null || body == "")) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Enter Description!')
                            .position('bottom left')
                            .hideDelay(1000)
                    );
                } else {

                    var params = {
                        id: id,
                        title: title,
                        body: body
                    };

                    var token = userService.getCurrentToken();
                    console.log(params);
                    HttpPostServices.postData(HttpPostServices.endpointEditBlog, params, token,
                        function(response) {
                            console.log("Success: " + response);
                        },
                        function(response) {
                            console.log("Error: " + response);
                        }
                    );

                    $mdDialog.hide(answer);
                }
            } else if(answer == 'close') {
                $mdDialog.hide(answer);
            }
        } else {
            $mdDialog.hide('logout');
            userService.logout();
            $location.path('/admin');
        }

    };

    $scope.tinymceOptions = {
        plugins: 'link image code',
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code',
        skin: 'lightgray',
        theme : 'modern',
        file_browser_callback: function(field_name, url, type, win) {
            console.log('file_browser_callback called!');
        }
    };
}]);

weddingProjectControllers.controller('DeleteBlogPopUpController', ['$scope', '$rootScope', 'HttpPostServices', 'userService', '$mdDialog', '$location', function ($scope, $rootScope, HttpPostServices, userService, $mdDialog, $location) {

    $scope.answer = function(answer) {
        if(userService.checkIfLoggedIn() == true) {
            if(answer == 'yes') {
                id = $rootScope.deleteBlogId;
                console.log(id);
                var token = userService.getCurrentToken();
                HttpPostServices.postData(HttpPostServices.endpointDeleteBlog + id, id, token,
                    function(response) {
                        console.log("Success: " + response);
                        $mdDialog.hide(answer);
                    },
                    function(response) {
                        console.log("Error: " + response);
                    }
                );
            } else if (answer == 'no') {
                $mdDialog.hide(answer);
            }
        } else {
            $mdDialog.hide('logout');
            userService.logout();
            $location.path('/admin');
        }
    };

}]);

weddingProjectControllers.controller('PortfolioAdminController', ['$scope', '$location', 'HttpGetServices', '$mdDialog', 'userService', 'parseHtmlService', function ($scope, $location, HttpGetServices, $mdDialog, userService, parseHtmlService) {

    if(userService.checkIfLoggedIn() == true) {

        $scope.currentNavItem = 'portfolio';

        HttpGetServices.getListData(HttpGetServices.endpointPortfolioList)
            .then(function(data) {
                $scope.portfolios = data;
            });

        $scope.parseHtml = function(data) {
            return parseHtmlService.parse(data);
        };

        $scope.addPortfolio = function(ev) {
              $mdDialog.show({
                controller: 'AddPortfolioPopUpController',
                templateUrl: 'partials/admin/popups/add-portfolio-pop-up.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: $scope.customFullscreen
            })
            .then(function(answer) {
              $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.status = 'You cancelled the dialog.';
            });
        };

        $scope.editPortfolio = function(ev) {
              $mdDialog.show({
                controller: 'EditPortfolioPopUpController',
                templateUrl: 'partials/admin/popups/edit-portfolio-pop-up.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: $scope.customFullscreen
            })
            .then(function(answer) {
              $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.status = 'You cancelled the dialog.';
            });
        };

        $scope.deletePortfolio = function(ev) {
              $mdDialog.show({
                controller: 'DeletePortfolioPopUpController',
                templateUrl: 'partials/admin/popups/delete-portfolio-pop-up.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: $scope.customFullscreen
            })
            .then(function(answer) {
              $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.status = 'You cancelled the dialog.';
            });
        };

    } else {
        userService.logout();
        $location.path('/admin');
    }

}]);

weddingProjectControllers.controller('AddPortfolioPopUpController', ['$scope', 'HttpPostServices', '$mdDialog', function ($scope, HttpPostServices, $mdDialog) {

    $scope.addPortfolio = function() {
        var title = $scope.title;
        var description = $scope.tinymceModel;

        var params = {
            title: title,
            description: description
        };

        var token = userService.getCurrentToken();
        console.log(params);
        HttpPostServices.postData(HttpPostServices.endpointAddNewPortfolio, params, token,
            function(response) {
                console.log("Success: " + response);
            },
            function(response) {
                console.log("Error: " + response);
            }
        );

    }

    $scope.close = function() {
        $mdDialog.hide();
    }

    $scope.tinymceOptions = {
        plugins: 'link image code',
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code',
        skin: 'lightgray',
        theme : 'modern',
        file_browser_callback: function(field_name, url, type, win) {
            console.log('file_browser_callback called!');
        }
    };

}]);

weddingProjectControllers.controller('EditPortfolioPopUpController', ['$scope', '$mdDialog', function ($scope, $mdDialog) {
    $scope.close = function() {
        $mdDialog.hide();
    }
}]);

weddingProjectControllers.controller('DeletePortfolioPopUpController', ['$scope', '$mdDialog', function ($scope, $mdDialog) {
    $scope.close = function() {
        $mdDialog.hide();
    }
}]);

weddingProjectControllers.controller('SliderAdminController', ['$scope', '$location', 'userService', function ($scope, $location, userService) {

    if(userService.checkIfLoggedIn() == true) {
        $scope.currentNavItem = 'slider';
    } else {
        userService.logout();
        $location.path('/admin');
    }

}]);

weddingProjectControllers.controller('CalendarAdminController', ['$scope', '$location','$mdDialog', 'userService', function ($scope, $location,$mdDialog, userService) {

    if(userService.checkIfLoggedIn() == true) {

        $scope.currentNavItem = 'calendar';

        $scope.dayClick = function(ev) {
              $mdDialog.show({
                controller: 'DayChangesBlogPopUpController',
                templateUrl: 'partials/admin/popups/reserve-day-pop-up.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: $scope.customFullscreen
            })
            .then(function(answer) {
              $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.status = 'You cancelled the dialog.';
            });
        };

    } else {
        userService.logout();
        $location.path('/admin');
    }

}]);
weddingProjectControllers.controller('DayChangesBlogPopUpController', ['$scope', '$mdDialog', function ($scope, $mdDialog) {
    
    $scope.close = function() {
        $mdDialog.hide();
    }

    $scope.tinymceOptions = {
        plugins: 'link image code',
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code',
        skin: 'lightgray',
        theme : 'modern',
        file_browser_callback: function(field_name, url, type, win) {
            console.log('file_browser_callback called!');
        }
    };
}]);
