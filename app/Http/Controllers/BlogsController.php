<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use JWTAuth;

use App\Http\Requests;

class BlogsController extends Controller
{
    public function getAllBlogs()
    {

        $blog = Blog::all();
        return response()->json($blog);
    }

    public function getBlog(Request $request)
    {
        $blog = Blog::find($request->id);
        return response()->json($blog);
    }

    public function postBlog (Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if($currentUser == null) {
            return "not logged in!";
        }

        $blog = new Blog();
        $blog->title = $request->title;
        $blog->body = $request->body;
        return $blog->save();
    }

    public function updateBlog (Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if($currentUser == null) {
            return "not logged in!";
        }

        $blog = Blog::find($request->id);
        $blog->title = $request->title;
        $blog->body = $request->body;
        return $blog->save();
    }

    public function deleteBlog(Request $request) {

        /*$currentUser = JWTAuth::parseToken()->authenticate();
        if($currentUser == null) {
            return "not logged in!";
        }*/

        $blog = Blog::find($request->id);

        $isDeleted = $blog->delete();

        if($isDeleted) {
            return "true";
        } else {
            return "true";
        }
    }
}
