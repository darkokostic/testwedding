<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Portfolio;
use JWTAuth;

use App\Http\Requests;

class PortfoliosController extends Controller
{
    public function getAllPortfolios()
    {
        $portfolio = Portfolio::all();
        return response()->json($portfolio);
    }

    public function getPortfolio(Request $request)
    {
        $portfolio = Portfolio::find($request->id);
        return response()->json($portfolio);
    }

    public function postPortfolio (Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if($currentUser == null) {
            return "not logged in!";
        }

        $portfolio = new Portfolio();
        $portfolio->title = $request->title;
        $portfolio->description = $request->description;
        $portfolio->image_url = $request->image_url;
        return $portfolio->save();
    }

    public function updatePortfolio (Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if($currentUser == null) {
            return "not logged in!";
        }

        $portfolio = Portfolio::find($request->id);
        $portfolio->title = $request->title;
        $portfolio->description = $request->description;
        if($request->image_url != null) {
            $portfolio->image_url = $request->image_url;
        } else {
            $portfolio->image_url = "";
        }

        return $portfolio->save();
    }

    public function deletePortfolio(Request $request) {

        $currentUser = JWTAuth::parseToken()->authenticate();
        if($currentUser == null) {
            return "not logged in!";
        }

        $portfolio = Portfolio::find($request->id);
        $isDeleted = $portfolio->delete();

        if($isDeleted) {
            return "true";
        } else {
            return "true";
        }
    }
}
