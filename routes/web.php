<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//Get all
Route::get('/api/blogs', 'BlogsController@getAllBlogs');
Route::get('/api/portfolios', 'PortfoliosController@getAllPortfolios');
// Get by ID
Route::get('/api/blogs/{id}', 'BlogsController@getBlog');
Route::get('/api/portfolios/{id}', 'PortfoliosController@getPortfolio');
Route::post('/login', 'LoginController@doLogin');

//Add
Route::post('/api/blogs/add', 'BlogsController@postBlog');
Route::post('/api/portfolios/add', 'PortfoliosController@postPortfolio');

//Update
Route::post('/api/blogs/update', 'BlogsController@updateBlog');
Route::post('/api/portfolios/update', 'PortfoliosController@updatePortfolio');

//Delete
Route::post('/api/blogs/delete/{id}', 'BlogsController@deleteBlog');
Route::post('/api/portfolios/delete/{id}', 'PortfoliosController@deletePortfolio');