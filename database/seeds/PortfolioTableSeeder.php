<?php

use Illuminate\Database\Seeder;
use App\Portfolio;

class PortfolioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('portfolios')->delete();

        Portfolio::create(array(
            'title'     => 'Portfolio 01',
            'description'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
            'image_url'    => '',
        ));

        Portfolio::create(array(
            'title'     => 'Portfolio 02',
            'description'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
            'image_url'    => '',
        ));

        Portfolio::create(array(
            'title'     => 'Portfolio 03',
            'description'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
            'image_url'    => '',
        ));

        Portfolio::create(array(
            'title'     => 'Portfolio 04',
            'description'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
            'image_url'    => '',
        ));

        Portfolio::create(array(
            'title'     => 'Portfolio 05',
            'description'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
            'image_url'    => '',
        ));

        Portfolio::create(array(
            'title'     => 'Portfolio 06',
            'description'    => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat aliquam dui, porta aliquet lectus tempor eget. Duis justo orci, placerat pellentesque pretium id, hendrerit vel mi. Phasellus ac nisl sed odio accumsan rutrum quis id tortor. Aliquam erat volutpat. Mauris a tincidunt quam. In maximus felis odio, eget sollicitudin ex pellentesque vel. Praesent fermentum in lectus non varius. Suspendisse condimentum, leo eget cursus porttitor, risus libero bibendum turpis, sed tincidunt orci nisl eget purus. Pellentesque eu fringilla magna.',
            'image_url'    => '',
        ));
    }
}
